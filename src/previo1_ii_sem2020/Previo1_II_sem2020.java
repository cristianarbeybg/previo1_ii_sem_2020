/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package previo1_ii_sem2020;

import java.util.Scanner;

/**
 * 
 * @author Cristian Bustos - 1151281
 */
public class Previo1_II_sem2020 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int matriz[][] = leerMatriz();
        impMatriz(matriz);
        impr_Num_Faltante(matriz);

    }

    private static void impr_Num_Faltante(int matriz[][]) {
        // :)
        int numero_faltante = 0;
        int sumarElementos = 0;
        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                // System.out.print(matriz[c][f]);
                if (c != matriz[f].length - 1) {
                    sumarElementos += matriz[c][f];
                    // System.out.print("\t");
                } else {
                    sumarElementos += matriz[c][f];
                    numero_faltante = sumarElementos / 2;
                    System.out.println("El numero faltante de la columna es: " + numero_faltante);
                }
            }
        }
    }

    /*
     * Suma de elementos
     */
    public static int sumarColumna(int [][] matriz) {
        int sumc = 0;
        for (int i = 0; i < matriz[0].length; i++) {
            sumc = 0;
            for (int j = 0; j < matriz.length; j++) {
                sumc = sumc + matriz[j][i];
            }
            System.out.println("La suma de los valores de la columna " + (i + 1) + " es: " + sumc);
        }
        return sumc;
    }

    private static int[][] leerMatriz() {
        Scanner sc = new Scanner(System.in);
        int filas, columnas;
        System.out.println("Digite cantidad de filas:");
        filas = sc.nextInt();
        System.out.println("Digite cantidad de columnas:");
        columnas = sc.nextInt();
        int matriz[][] = new int[0][0];

        if (columnas % 2 == 1) {
            matriz = new int[filas][columnas];
            for (int i = 0; i < filas; i++) {
                for (int j = 0; j < columnas; j++) {
                    System.out.println("Por favor digite el elemento de la matriz[" + i + "][" + j + "]:");
                    matriz[i][j] = sc.nextInt();
                }
            }
        } else {
            System.out
                    .println("ERROR! No se puede realizar el proceso \n La cantidad de columnas deben ser impares \n");
        }
        return matriz;

    }

    private static void impMatriz(int matriz[][]) {
        System.out.println("Imprimiendo mi matriz:");
        String msg = "";
        for (int vector[] : matriz) {
            for (int dato : vector)
                msg += dato + "\t";
            msg += "\n";
        }
        System.out.println(msg);
    }
}
